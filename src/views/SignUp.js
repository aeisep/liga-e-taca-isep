import * as React from 'react'
import '../assets/css/SignUp.css';
import { Button } from 'antd';
import { FilePdfOutlined } from '@ant-design/icons';
import Regulation from '../assets/docs/Regulamento_2024.pdf';

function SignUp() {
    return (
        <div id="signup">
            <h1 className="title">Inscrição</h1>
            <h3 className="semibold-text">Consulta o Regulamento antes de procederes com a inscrição!</h3>
            <a href={Regulation} target="_blank" rel="noreferrer">
                <FilePdfOutlined id="regulation-icon" />
                <p id="file-name" className="semibold-text">Regulamento Liga & Taça ISEP</p>
            </a>
            <h2 className="bold-text">Escolhe a tua plataforma!</h2>
            <div id="platform-div">
                <Button id="pc-btn" className="platform-btn" href="https://bit.ly/381Pzpm" target="_blank"> </Button>
                <Button id="ps-btn" className="platform-btn" href="https://bit.ly/3rbR8J4" target="_blank"> </Button>
            </div>
            <h4 id="declaration-text" className="regular-text">
                <b>Nota: </b>
                Ao proceder com a inscrição, não só declaras que
                tomaste conhecimento do regulamento estabelecido,
                mas também que permites a utilização dos teus dados
                no decorrer do evento.
            </h4>
        </div >
    );
}

export default SignUp;